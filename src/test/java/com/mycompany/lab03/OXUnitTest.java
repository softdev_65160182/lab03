/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author PT
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWin_O_row1_output_true(){
        String[][] table = {{"O", "O", "O"},{"-", "-", "-"},{"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_row2_output_true(){
        String[][] table = {{"-", "-", "-"},{"O", "O", "O"},{"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_row3_output_true(){
        String[][] table = {{"-", "-", "-"},{"-", "-", "-"},{"O", "O", "O"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_O_col1_output_true(){
        String[][] table = {{"O", "-", "-"},{"O", "-", "-"},{"O", "-", "-"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_O_col2_output_true(){
        String[][] table = {{"-", "O", "-"},{"-", "O", "-"},{"-", "O", "-"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_O_col3_output_true(){
        String[][] table = {{"-", "-", "O"},{"-", "-", "O"},{"-", "-", "O"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_O_crossR2L_output_true(){
        String[][] table = {{"-", "-", "O"},{"-", "O", "-"},{"O", "-", "-"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_crossL2R_output_true(){
        String[][] table = {{"O", "-", "-"},{"-", "O", "-"},{"-", "-", "O"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_row1_output_true(){
        String[][] table = {{"X", "X", "X"},{"-", "-", "-"},{"-", "-", "-"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_row2_output_true(){
        String[][] table = {{"-", "-", "-"},{"X", "X", "X"},{"-", "-", "-"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_row3_output_true(){
        String[][] table = {{"-", "-", "-"},{"-", "-", "-"},{"X", "X", "X"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
        
    @Test
    public void testCheckWin_X_col1_output_true(){
        String[][] table = {{"X", "-", "-"},{"X", "-", "-"},{"X", "-", "-"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_col2_output_true(){
        String[][] table = {{"-", "X", "-"},{"-", "X", "-"},{"-", "X", "-"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_col3_output_true(){
        String[][] table = {{"-", "-", "X"},{"-", "-", "X"},{"-", "-", "X"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_crossR2L_output_true(){
        String[][] table = {{"-", "-", "X"},{"-", "X", "-"},{"X", "-", "-"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_crossL2R_output_true(){
        String[][] table = {{"X", "-", "-"},{"-", "X", "-"},{"-", "-", "X"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_O_row3_output_false(){
        String[][] table = {{"-", "-", "-"},{"-", "-", "-"},{"O", "O", "-"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWin_O_row2_output_false(){
        String[][] table = {{"-", "-", "-"},{"O", "O", "-"},{"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWin_X_crossL2R_output_false(){
        String[][] table = {{"-", "-", "-"},{"-", "X", "-"},{"-", "-", "X"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
}
