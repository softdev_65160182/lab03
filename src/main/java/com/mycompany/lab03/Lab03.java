/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab03;

import java.util.Scanner;

/**
 *
 * @author PT
 */
public class Lab03 {
    static boolean checkWin(String[][] table, String currentPlayer) {
        if(checkRow(table,currentPlayer)){
            return true;
        }
        if(checkCol(table,currentPlayer)){
            return true;
        }
        if(checkCrossR2L(table,currentPlayer)){
            return true;
        }
        if(checkCrossL2R(table,currentPlayer)){
            return true;
        }
        return false;
    }
    
    private static boolean checkRow(String[][] table, String currentPlayer) {
            for(int row =0;row<3;row++){
            if(table[row][0].equals(table[row][1])&&table[row][1].equals(table[row][2])){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }
    
    private static boolean checkCol(String[][] table, String currentPlayer) {
        for(int col =0;col<3;col++){
            if(table[0][col].equals(table[1][col])&&table[1][col].equals(table[2][col])){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }
    
    private static boolean checkCrossR2L(String[][] table, String currentPlayer) {
        for(int col =0;col<3;col++){
            if(table[0][2].equals(table[1][1])&&table[1][1].equals(table[2][0])){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }
    
    private static boolean checkCrossL2R(String[][] table, String currentPlayer) {
        for(int col =0;col<3;col++){
            if(table[0][0].equals(table[1][1])&&table[1][1].equals(table[2][2])){
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }
}
